# Project

I had to create a game in vanilla js using classes, i decided to level up the complexity of the project because you know, why not ?
So I made a very simplistic 2D RPG (which I obviously didn't finished because I had to learn a lot of things in not so much time, there isn't so many tuto on youtube for this type of game in vanilla js ngl) where you play as a cat.
I didn't add a backstory (yet ?) to the game but I think if I had to finish the project it would be a cat missing a soul who'll try to get it back by every means to be able to feel things again, the end will be sad, u'll cry, anyway if I decide to finish the game I'll post the updated version here.

## Realisation

I did two wireframes, one for the global idea and one who act somewhat as a functional prototype :

[![First_wireframe.png](Readme_img/First_wireframe.png)](https://whimsical.com/game-3XSTTR6nnp9tPfryU3KyHF)

[![Second_wireframe.png](Readme_img/Second_wireframe.png)](https://whimsical.com/game-3XSTTR6nnp9tPfryU3KyHF)

I used Bootstrap for the positionning of every element, the game is based on canvas with a function to refresh the canvas a specified number of times per seconds to simulate a framerate.

There is a lot of things I made to make it easier to create things in the game, to do that I mainly used classes, there is one for every little things I had to use often like interact points or backgrounds,
for the collision I made a collision map with 0 and 1 (screen below) that I also used to create my items with a loop who search for other numbers than the two mentionned before and create elements based on that number.

![map.png](Readme_img/map.png)

## Link to the actual version + TL;DR

Fun project, learned a lot, game is in early early stage, see ya space cowboy.

[LINK](https://kevinrsi.gitlab.io/game/)
