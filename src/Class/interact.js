//Create the items which the player will interact with to do multiple things like changing panel or advance the story
export class Interact{
    sprite = new Image();
    gridX = '';
    gridY = '';
    width = '';
    height = '';
    constructor(x, y, src, w, h){
        this.sprite.src = src;
        this.gridX = x;
        this.gridY = y;
        this.width = w;
        this.height = h;
    }
}