import { currentBackground } from "../app";
//create the player and the function to determine his position on the canvas
export class Player {
    x = 0;
    y = -5;
    width = 5;
    height = 5;
    speed = 4;
    moving = false;
    frameX = [1,0,2];
    frameY = 0;
    interact = false;
    sprite = new Image();
    constructor(w,h, src){
        this.sprite.src = src;
        this.width = w;
        this.height = h;
    };
    /**
     * 
     * @returns the position of the player in base 16 to match the square defined in the map array
     */
    gridPosX(){
        return Math.floor(this.x/currentBackground.squareSize)+1;
    }
    gridPosY(){
        return Math.floor(this.y/currentBackground.squareSize)+1;
    }
}