//control the movement and everything related to it
const keys = [];
const facingDown = 0;
const facingUp = 3;
const facingLeft = 1;
const facingRight = 2;

export let paused = false;
let posX = 0;
let posY = 0;
import {
    changeBg,
    currentBackground,
    p1,
    ctx
} from '../app.js';


/**
 * allow the player to move when specified keys are pressed
 */

export function move() {
    let gridCollisionX = p1.gridPosX();
    let gridCollisionY = p1.gridPosY();
    const upGrid = currentBackground.map[gridCollisionY - 1][gridCollisionX];
    const rightGrid = currentBackground.map[gridCollisionY][gridCollisionX + 1];
    const downGrid = currentBackground.map[gridCollisionY + 1][gridCollisionX];
    const leftGrid = currentBackground.map[gridCollisionY][gridCollisionX - 1];
    const perfectGrid = currentBackground.map[gridCollisionY][gridCollisionX];
    if ((keys["ArrowUp"] || keys["z"])) {
        if (upGrid != 1 && p1.y > 0) {
            p1.y -= p1.speed;
            p1.frameY = facingUp;
            p1.moving = true;
        }
    } else if ((keys["ArrowDown"] || keys["s"])) {
        if (downGrid != 1 && p1.y < 310) {
            p1.y += p1.speed;
            p1.frameY = facingDown;
            p1.moving = true;
        }
    } else if ((keys["ArrowRight"] || keys["d"])) {
        if (rightGrid != 1 && typeof rightGrid === 'number') {
            p1.x += p1.speed;
            p1.frameY = facingRight;
            p1.moving = true;
        }
    } else if ((keys["ArrowLeft"] || keys["q"])) {
        if (leftGrid != 1 && typeof rightGrid === 'number') {
            p1.x -= p1.speed;
            p1.frameY = facingLeft;
            p1.moving = true;
        }
    } else if (keys["Enter"]) {
        if (perfectGrid === 3) {
            changeBg(1, 'next');
            p1.interact = true;
            console.log('found ya');
        } else if ((perfectGrid || upGrid) === 4) {
            changeBg(-1, 'return');
            p1.interact = true;
            console.log('found ya');
        }
        
    }

}

export function stopK(){
    if(keys["Escape"]){
        paused = true;
        ctx.fillStyle = '#FFFFFF'
        ctx.font = '48px serif';
        ctx.fillText('PAUSED', 155, 160);
        ctx.font = '24px serif';
        ctx.fillText('Press enter to return to the game', 90, 180);
    } else if(keys['Enter']){
        paused = false;
    }
}


window.addEventListener('keydown', (e) => {
    keys[e.key] = true;
    console.log(p1.gridPosX(), p1.gridPosY());
})

window.addEventListener('keyup', (e) => {
    delete keys[e.key];
    p1.moving = false;
    p1.interact = false;
})