export class BackgroundImg {
    img = new Image();
    width = 0;
    height = 0;
    map = "";
    squareSize = 16;
    x= 0;
    y = 0;
    constructor(src, w, h, map, x, y){
        this.img.src = src;
        this.width = w;
        this.height = h;
        this.map = map;
        this.x = x;
        this.y = y;
    };
}
