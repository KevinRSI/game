const canvas = document.querySelector("#canvas");
const start = document.querySelector('.start');
const title = document.querySelector('.title');
export const ctx = canvas.getContext('2d');
const width = 480;
const height = 320;
let items = [];

let indexFrame = 0;
let time = {
    fps: 20,
    fpsInterval: 0,
    startTime: 0,
    timeNow: 0,
    timeThen: 0,
    elapsed: 0
}
/*Import*/
import {
    move, stopK, paused
} from './Class/keys.js';

import {
    Player
} from './Class/player.js';

import {
    BackgroundImg
} from './Class/background.js';

import {
    map1
} from './maps/map1.js';

import {
    Interact
} from './Class/interact.js';

import {
    map2
} from './maps/map2.js';

/*Export*/
export let bg = [new BackgroundImg('assets/floor1.png', 791, 575, map1, 0, 0), new BackgroundImg('assets/floor1.png', 791, 255, map2, 0, -320)]

export let p1 = new Player(48, 48, 'sprites/cats.png');
let indexBG = 0
export let currentBackground = bg[indexBG];

/**
 * Create the items defined on the maps based on their numbers and push them in an array to be drawn
 */
function createItems() {
    for (let index = 0; index < currentBackground.map.length; index++) {
        if (currentBackground.map[index].indexOf(3) != -1) {
            let gridInteract = currentBackground.map[index].indexOf(3);
            items.push(new Interact(gridInteract, index, 'assets/nether_star.png', 16, 16));
        } else if (currentBackground.map[index].indexOf(4) != -1) {
            let gridInteract = currentBackground.map[index].indexOf(4);
            items.push(new Interact(gridInteract, index, 'assets/nether_star.png', 30, 30));
        }

    }
}
canvas.style.display = 'none'
/**
 * change the 'map' by adding 1 to the index, also clear the items array to refresh the list
 */
export function changeBg(a, state) {
    if (p1.interact === false) {
        if (state === 'next') {
            indexBG += a;
            currentBackground = bg[indexBG];
            console.log(currentBackground);
            items = [];
            startBgPos();
        }
        else if(state === 'return'){
            indexBG += a;
            currentBackground = bg[indexBG];
            console.log(currentBackground);
            items = [];
            returnBgPos();
        }

    }

}
/**
 * Pause tha game and show a message
 */
export function gameStop(){
    if (paused === true) {
        paused = false;
    } else {
        paused = true;
        ctx.font = '48px serif';
ctx.fillText('PAUSED', 155, 160);

    }
}

canvas.width = width;
canvas.height = height;


/**
 * draw the player based on the defined sprite by cropping the desired areaand defining it's size on the screen
 * @param {string} img 
 * @param {number} sX 
 * @param {number} sY 
 * @param {number} sW 
 * @param {number} sH 
 * @param {number} dX 
 * @param {number} dY 
 * @param {number} dW 
 * @param {number} dH 
 */
function drawSprite(img, sX, sY, sW, sH, dX, dY, dW, dH) {
    ctx.drawImage(img, sX, sY, sW, sH, dX, dY, dW, dH);
}


/**
 * give the position the player has when he enter the map
 */
function startBgPos() {
    if (indexBG === 0) {
        p1.x = 87;
        p1.y = 65;
    } else if (indexBG === 1) {
        p1.x = 130;
        p1.y = 0;
    }
}
/**
 * give the position the player has when he return to the map
 */
function returnBgPos() {
    if (indexBG === 0) {
        p1.x = 130;
        p1.y = 280;
    }
    
}




/**
 * tell the drawsprite function to change column on the player sprite when moving so it feels animated
 */
function animatePlayer() {
    if (p1.moving && indexFrame < 2) {
        indexFrame++;
    } else {
        indexFrame = 0;
    }
}

/**Work with refresh to animate the canvas with a specified framerate so that it stay the same on every monitor */
function startAnimation(fps) {
    time.fpsInterval = 1000 / fps;
    time.timeThen = Date.now();
    time.startTime = time.timeThen;
    startBgPos();
    refresh();
}
/** refresh the canvas and the function needed to animate the game  */
function refresh() {
    stopK();
    if (paused === false) {
        time.timeNow = Date.now();
    time.elapsed = time.timeNow - time.timeThen;
    if (time.elapsed > time.fpsInterval) {
        time.timeThen = time.timeNow - (time.elapsed % time.fpsInterval);
        move();
        createItems();
        animatePlayer();
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        drawSprite(currentBackground.img, 0, 10, 480 * 1.64, 320 * 1.79, currentBackground.x, currentBackground.y, 480 * 1.64, 320 * 1.79)
        for (let i = 0; i < items.length; i++) {
            const element = items[i];
            drawSprite(element.sprite, 0, 0, element.width, element.height, element.gridX * currentBackground.squareSize, element.gridY * currentBackground.squareSize, element.height, element.width);
        }
        drawSprite(p1.sprite, p1.width * p1.frameX[indexFrame], p1.height * p1.frameY, p1.width, p1.height, p1.x, p1.y, p1.width / 3, p1.height / 3);
    }
    }
    requestAnimationFrame(refresh);
}



window.onload = function startGame() {
    start.addEventListener('click', () => {
        if (canvas.style.display === 'none') {
            canvas.style.display = 'block';
            title.style.display = 'none'
            start.style.display = 'none';
            startAnimation(time.fps);
        }
    })

}